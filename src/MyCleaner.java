
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

//Klasa do czyszczenia stringa do API

public  class MyCleaner extends WindowAdapter {

    private String chartName;
    MyCleaner(String text){
        chartName = text;
    }
    @Override
    public void windowClosing(WindowEvent windowEvent) {
        Logger logger = org.apache.log4j.Logger.getLogger(this.getClass().getName());
        DownloadFile.clearURI();
        windowEvent.getWindow().dispose();

        logger.info("Chart of " + chartName + " closed ");
    }


}