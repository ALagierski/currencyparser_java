import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//Klasa do pokazywania aktualnej wartosci kursu dla danej waluty wybranej z listy rozsuwalnej
class MyActionListener implements ActionListener
{
    private JLabel njLabel;
    private JLabel njField;


    MyActionListener(JLabel jlab,JLabel jtf){
        njLabel = jlab;
        njField = jtf;
    }



    public void actionPerformed(ActionEvent event) {
        Logger logger = org.apache.log4j.Logger.getLogger(this.getClass().getName());
        JComboBox comboChoice = (JComboBox) event.getSource();

        String listChoice =(String) comboChoice.getSelectedItem();

        logger.info("New currency chosen: " + listChoice);
       // System.out.println(listChoice);

        //Na podstawie flagi, wykonaj akcje
        if(Parser.getTable()) {
            if (Parser.ifOnList(listChoice)) {
                int listenedIndex = Parser.getSymbols().indexOf(listChoice);
                double currencyRate = (double) Parser.getValues().get(listenedIndex);
                njLabel.setText("Today's currency rate: \n" + currencyRate + " PLN");
                njField.setText("Name (pl): "+Parser.getNames().get(listenedIndex));
            }
        }
        else
        {
            if (Parser.ifOnListB(listChoice)) {
                int listenedIndex = Parser.getSymbolsB().indexOf(listChoice);
                double currencyRate = (double) Parser.getValuesB().get(listenedIndex);
                njLabel.setText("Today's currency rate: \n" + currencyRate + " PLN");
                njField.setText("Name (pl): "+Parser.getNamesB().get(listenedIndex));
            }
        }
    }
}
