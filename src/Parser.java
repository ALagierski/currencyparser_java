import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;


// Klasa sluzaca do parsowania aktualnych wartosci
class Parser  {
    //Tablice zawierajace nazwy, wartosci oraz symbole
    private static ArrayList<String> symbols = new ArrayList<>();
    private static ArrayList<Double> values = new ArrayList<>();
    private static ArrayList<String> symbolsB = new ArrayList<>();
    private static ArrayList<Double> valuesB = new ArrayList<>();
    private static ArrayList<String> names = new ArrayList<>();
    private static ArrayList<String> namesB = new ArrayList<>();
    //flaga do przechodzenia miedzy tablicami
    private static  boolean ifA =true;

    //Zeby raz parswowac :D
    Parser() throws IOException
    {
        parser();
    }

    private static void parser() throws IOException
    {
        //Łączenie z odpowiednim linkiem
        String currencyTable = "https://www.nbp.pl/home.aspx?f=/kursy/kursya.html";
        String currencyTableB = "https://www.nbp.pl/home.aspx?f=/kursy/kursyb.html";
        Document page;
        Document pageB;
        page = Jsoup.connect(currencyTable).timeout(10000).get(); //10 sec of timeout
        pageB = Jsoup.connect(currencyTableB).timeout(10000).get();


        //Zbieranie danych poprzez CSSQuery
        Elements symbolCol = page.select("td.bgt1.right:eq(1), td.bgt2.right:eq(1)");
        Elements valueCol = page.select("td.bgt1.right:eq(2), td.bgt2.right:eq(2)");
        Elements namesCol = page.select("td.bgt1.left, td.bgt2.left");

        Elements symbolColB = pageB.select("td.bgt1.right:eq(1), td.bgt2.right:eq(1)");
        Elements valueColB = pageB.select("td.bgt1.right:eq(2), td.bgt2.right:eq(2)");
        Elements namesColB = pageB.select("td.bgt1.left, td.bgt2.left,td.bgt1i.left,td.bgt2i");


        for (Element src : symbolCol)
        {
            symbols.add(src.text());
        }

        for (Element src : valueCol)
        {

            values.add(Double.parseDouble(src.text().replace(',','.')));
        }

        for (Element src : symbolColB)
        {
            symbolsB.add(src.text());
        }

        for (Element src : valueColB)
        {

            valuesB.add(Double.parseDouble(src.text().replace(',','.')));
        }

        for(Element src : namesCol)
        {
            names.add(src.text());
        }

        for(Element src : namesColB)
        {
            namesB.add(src.text().replace("1)",""));
        }




    }

    // Settery i gettery
    static ArrayList getSymbols() {

        return Parser.symbols;
    }

    static ArrayList getValues() {

        return Parser.values;
    }
    static ArrayList getSymbolsB() {

        return Parser.symbolsB;
    }

    static ArrayList getValuesB() {

        return Parser.valuesB;
    }

    static boolean ifOnList(String str)
    {
        return symbols.contains(str);
    }
    static boolean ifOnListB(String str)
    {
        return symbolsB.contains(str);
    }

    static void setTable(boolean change)
    {
        ifA = change;
    }
    static boolean getTable()
    {
        return ifA;
    }

    static ArrayList getNames()
    {
        return Parser.names;
    }

    static ArrayList getNamesB()
    {
        return Parser.namesB;
    }

}
