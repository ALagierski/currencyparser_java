import javax.swing.*;
import java.awt.*;
import java.io.IOException;

class GUI {
    static void createAndShowGUI() throws IOException {
        //Jedno parsowanie podczas tworzenia GUI
        new Parser();

        //Generalnie tworze GUI. Niektore elemenety zawieraja listenery
        String[] guiSymbols;
        guiSymbols = (String[]) Parser.getSymbols().toArray(new String[0]);
        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame jGUI = new JFrame("Currency rate");
        jGUI.setLayout(new GridLayout(3,2,5,5));


        JPanel jpN = new JPanel();
        jpN.setLayout(new GridLayout(1,2));
        JPanel jpC = new JPanel();
        jpC.setLayout(new GridLayout(1,1));

        JPanel jpB = new JPanel();
        jpB.setLayout(new GridLayout(1,2));

        JLabel jLabel = new JLabel("Today's currency rate: \n" + Parser.getValues().get(0) + " PLN");
        jLabel.setHorizontalAlignment(JLabel.RIGHT);
        jLabel.setVerticalAlignment(JLabel.CENTER);


        JLabel jtf = new JLabel("Name (pl): "+Parser.getNames().get(0),SwingConstants.CENTER);



        MyActionListener myActionListener = new MyActionListener(jLabel,jtf);

        JComboBox<String> jcb = new JComboBox<>(guiSymbols);


        jcb.setBounds(50,50,90,20);
        jcb.addActionListener(myActionListener);



        JButton jb = new JButton("Switch currency table");
        jb.addActionListener(new ButtonListener(jcb,jLabel,jtf));

        JButton jbGraph = new JButton("Draw monthly graph");
        jbGraph.addActionListener(new GraphicsListener(jcb));

        jGUI.getContentPane().add(jpN,BorderLayout.NORTH);
        jpN.add(jcb,BorderLayout.WEST);
        jpN.add(jLabel,BorderLayout.EAST);

        jGUI.getContentPane().add(jpC,BorderLayout.CENTER);
        jpC.add(jtf);
        jGUI.getContentPane().add(jpB,BorderLayout.SOUTH);
        jpB.add(jb,BorderLayout.WEST);
        jpB.add(jbGraph,BorderLayout.EAST);




        jGUI.pack();
        jGUI.setVisible(true);
        jGUI.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jGUI.addWindowListener(new AppCloser());
    }


    static String[] getGUISymbolsA() {
        return (String[]) Parser.getSymbols().toArray(new String[0]);
    }
    static String[] getGUISymbolsB() {
        return (String[]) Parser.getSymbolsB().toArray(new String[0]);
    }

}